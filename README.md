# LibreWolf Website

This is the main website for the [LibreWolf](https://gitlab.com/librewolf-community/librewolf) project.

![Build Status](https://gitlab.com/librewolf-community/librewolf-community.gitlab.io/badges/master/build.svg)

## Credits

* Some images by [Darwin Laganzon](https://pixabay.com/users/typographyimages-3575871/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1915628), [2998800](https://pixabay.com/users/2998800-2998800/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1915455) and [Pettycon](https://pixabay.com/users/Pettycon-3307648/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1674891) from [Pixabay](https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1674891)

* Icons made by [Pixel perfect](https://www.flaticon.com/authors/pixel-perfect), [Icon Monk](https://www.flaticon.com/authors/icon-monk) and [Smashicons](https://www.flaticon.com/authors/smashicons) from [www.flaticon.com](https://www.flaticon.com/) are licensed by [CC 3.0 BY](http://creativecommons.org/licenses/by/3.0/)
